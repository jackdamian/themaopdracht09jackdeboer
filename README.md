# Themaopdracht09JackdeBoer

Dit is de repository voor de themaopdracht van thema 9 gemaakt door Jack D de Boer. Bij dit onderzoek is er gekeken naar data van ziekhuis opnames van diabetes patienten. Bij deze dataset was er vermeld of de pati?nten na de opname nog een keer in het
ziekenhuis zijn opgenomen. Met behulp van machine learning is er een model gemaakt met het doel om te voorspellen of een bepaalde opgenomen diabetes patient later weer heropgenomen zou moeten worden. Verder
is er gekeken hoeveel attributen dit model maximaal nodig had om de mogelijke heropname zo accuraat mogelijk te voorspellen.
 

De repository bevat de directories data, docs en scripts:

	-De directory data bevat de datasets van het onderzoek.
	-De directory docs bevat het bijgehouden log.
	-De directory scripts bevat alle gemaakte scripts voor het onderzoek.
	
# Prerequisites
Voor dit project is Rstudio gebruikt. De .Rmd bestanden moeten met Rstudio geopend worden.

# License
# Copyright (c) 2018 Jack D. de Boer.
# Licensed under GPLv3. See gpl.md